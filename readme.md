# Nudge Technical Test

## How to run the app

Inside this folder run the following command

`php artisan serve`

then visit http://127.0.0.1:8000 in your web browser
## How to run the tests

Inside this folder run the following command

`vendor/bin/phpunit`
## How to install any dependencies required

Inside this folder run the following command

`composer install`
## Extra Information

#### Approach 

* Create a controller and route to output our Json
* Luckily black-ink.org has the WP-API plugin installed which provides a rest API at `/wp-json/` that we can use
* Abstract access to this API end-point using the repository design pattern, This will make our code easier to mock and 
provide a layer of abstraction from the wordpress API 
* Use the WordPressRepository we have created to build objects for each post and get the links from the posts
* Pass each link to the LinkMetaDataFactory, the factory will then visit the link and create a serilizable LinkMetaData 
object Again the factory provides us with model objects that make our code easy to mock, 
* Build an array of these objects and json encode it 

#### Potential Improvements 

* Using a large MVC framework for such a simple task is overkill, a microframe work would be better
* My solution is TOTALLY over engineered - I built it this way to try to show some of my OOD skills
* Inside LinkMetadataFactory I use `get_meta_tags()` to get the metatag data - I also use Guzzle to get the title, this 
means I make 2 HTTP requests, If performance was important I'd use guzzle and a DOMModel to get the metatags
* Inside LinkMetadataFactory i use `libxml_use_internal_errors()` to avoid errors when parsing html5, in production code
I would use an other document parser like html5-php
* The tests I have writen are not great and I did not follow the TDD mantra of writing tests first and using them as a
 contract

#### Version Control

* I have left my commits unsquished so you can see the process I took to build this project
* In other projects I like to create a branch and then use rebase to squish commits down to 1 commit per feature
* I have followed the angular commit message style but I have removed the scope part of the message since this is such a
small project
 
#### Dependencies

* Guzzle: Rest API Client
