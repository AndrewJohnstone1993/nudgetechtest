<?php

namespace Tests\Unit;

use Tests\TestCase;

class PostsJsonControllerTest extends TestCase
{
    public function testViewAction()
    {
        $response = $this->call('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($response->getContent());
    }
}
