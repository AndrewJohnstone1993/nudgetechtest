<?php

namespace Tests\Unit;

use App\Factory\LinkMetadataFactory;
use App\Model\LinkMetaData;
use Tests\TestCase;

class LinkMetaDataFactoryTest extends TestCase
{
    /**
     * @var $linkMetadataObject LinkMetaData
     */
    protected $linkMetadataObject;

    public function setUp()
    {
        $linkMetadataFactory = new LinkMetaDataFactory();
        $this->linkMetadataObject = $linkMetadataFactory->create('https://google.com');

        parent::setUp();
    }

    public function testCreate()
    {
        $this->assertInstanceOf('App\Model\LinkMetaData', $this->linkMetadataObject);
    }

    public function testMetaDescription()
    {
        $this->assertNotNull($this->linkMetadataObject->getMetaDescription());
    }

    public function testgetTitle()
    {
        $this->assertNotNull($this->linkMetadataObject->getMetaDescription());
    }

    public function testgetKeywords()
    {
        $this->assertNotNull($this->linkMetadataObject->getMetaDescription());
    }


    public function testgetSize()
    {
        $this->assertNotNull($this->linkMetadataObject->getMetaDescription());
    }

}
