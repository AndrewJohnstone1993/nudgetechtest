<?php

namespace Tests\Unit;

use App\Model\WordPressPost;
use App\Repository\PostsRepository;
use Tests\TestCase;

class PostsRepositoryTest extends TestCase
{
    public function testPostsTest()
    {
        $repository = new PostsRepository();
        $posts = $repository->getPosts();

        //Check posts array is not empty
        $this->assertNotEmpty($posts);

        //Check posts array is not null
        $this->assertNotNull($posts);

        //Check posts are correct type
        $this->assertInstanceOf('App\Model\WordPressPost',$posts[0]);
    }

    public function testPostsByCategoryTest()
    {
        $repository = new PostsRepository();
        $posts = $repository->getPostsByCategory(3);
        $this->assertTrue(true);

        //If we find any posts check they are the correct category
        foreach($posts as $post) {
            /**
             * @var WordPressPost $post
             */
            foreach ($post->getCategories() as $cat) {
                if ($cat != 3) {
                   $this->assertTrue(false);
                   break;
                }
            }
        }
    }


}
