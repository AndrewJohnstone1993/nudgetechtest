<?php

namespace App\Model;


use JsonSerializable;

class LinkMetaData implements JsonSerializable
{
    protected $url;
    protected $metaDescription;
    protected $title;
    protected $keywords;
    protected $size;

    public function __construct($url, $metaDescription, $title, $keywords, $size)
    {
        $this->setUrl($url);
        $this->setMetaDescription($metaDescription);
        $this->setTitle($title);
        $this->setKeywords($keywords);
        $this->setSize($size);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords($keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return float
     */
    public function getSize(): float
    {
        return $this->size;
    }

    /**
     * @param $sizeInBits
     */
    public function setSize($sizeInBits): void
    {
        $size = number_format($sizeInBits / 1024, 2);

        $this->size = $size;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
                'url' => $this->url,
                'metaDescription' => $this->metaDescription,
                'title' => $this->title,
                'keywords' => $this->keywords,
                'size' => $this->size . ' kb',
        ];
    }
}