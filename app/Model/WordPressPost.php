<?php

namespace App\Model;

use DOMDocument;
use Faker\Provider\zh_CN\DateTime;

class WordPressPost
{
    protected $id;
    protected $date;
    protected $date_gmt;
    protected $guid;
    protected $modified;
    protected $modified_gmt;
    protected $slug;
    protected $status;
    protected $type;
    protected $link;
    protected $title;
    protected $content;
    protected $excerpt;
    protected $author;
    protected $featured_media;
    protected $comment_status;
    protected $ping_status;
    protected $sticky;
    protected $template;
    protected $format;
    protected $meta;
    protected $categories;
    protected $tags;
    protected $_links;

    public function __construct($array)
    {
        $this->setId($array["id"]);
        $this->setDate($array["date"]);
        $this->setDateGmt($array["date_gmt"]);
        $this->setGuid($array["guid"]);
        $this->setModified($array["modified"]);
        $this->setModifiedGmt($array["modified_gmt"]);
        $this->setSlug($array["slug"]);
        $this->setStatus($array["status"]);
        $this->setType($array["type"]);
        $this->setLink($array["link"]);
        $this->setTitle($array["title"]);
        $this->setContent($array["content"]);
        $this->setExcerpt($array["excerpt"]);
        $this->setAuthor($array["author"]);
        $this->setFeaturedMedia($array["featured_media"]);
        $this->setCommentStatus($array["comment_status"]);
        $this->setPingStatus($array["ping_status"]);
        $this->setSticky($array["sticky"]);
        $this->setTemplate($array["template"]);
        $this->setFormat($array["format"]);
        $this->setMeta($array["meta"]);
        $this->setCategories($array["categories"]);
        $this->setTags($array["tags"]);
        $this->setLinks($array["_links"]);
    }

    /**
     * @return array
     */
    public function getUriFromContent(): array
    {
        $links = [];

        $dom = new DOMDocument();
        $dom->loadHTML($this->getContent()["rendered"]);

        $hyperLinks = $dom->getElementsByTagName("a");

        /**
         * @var $hyperLink \DOMElement
         */
        foreach($hyperLinks as $hyperLink) {
            $links[] = $hyperLink->getAttribute("href");
        }

        return $links;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return \DateTime
     */
    public function getDateGmt(): \DateTime
    {
        return $this->date_gmt;
    }

    /**
     * @param \DateTime $date_gmt
     */
    public function setDateGmt($date_gmt): void
    {
        $this->date_gmt = $date_gmt;
    }

    /**
     * @return array
     */
    public function getGuid(): array
    {
        return $this->guid;
    }

    /**
     * @param array $guid
     */
    public function setGuid($guid): void
    {
        $this->guid = $guid;
    }

    /**
     * @return \DateTime
     */
    public function getModified(): \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified): void
    {
        $this->modified = $modified;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedGmt(): \DateTime
    {
        return $this->modified_gmt;
    }

    /**
     * @param DateTime $modified_gmt
     */
    public function setModifiedGmt($modified_gmt): void
    {
        $this->modified_gmt = $modified_gmt;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @param array $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param array $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getExcerpt(): array
    {
        return $this->excerpt;
    }

    /**
     * @param array $excerpt
     */
    public function setExcerpt($excerpt): void
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @return int
     */
    public function getAuthor(): int
    {
        return $this->author;
    }

    /**
     * @param int $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getFeaturedMedia(): int
    {
        return $this->featured_media;
    }

    /**
     * @param int $featured_media
     */
    public function setFeaturedMedia($featured_media): void
    {
        $this->featured_media = $featured_media;
    }

    /**
     * @return string
     */
    public function getCommentStatus(): string
    {
        return $this->comment_status;
    }

    /**
     * @param string $comment_status
     */
    public function setCommentStatus($comment_status): void
    {
        $this->comment_status = $comment_status;
    }

    /**
     * @return string
     */
    public function getPingStatus(): string
    {
        return $this->ping_status;
    }

    /**
     * @param string $ping_status
     */
    public function setPingStatus($ping_status): void
    {
        $this->ping_status = $ping_status;
    }

    /**
     * @return bool
     */
    public function getSticky(): bool
    {
        return $this->sticky;
    }

    /**
     * @param bool $sticky
     */
    public function setSticky($sticky): void
    {
        $this->sticky = $sticky;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format): void
    {
        $this->format = $format;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @param array $meta
     */
    public function setMeta($meta): void
    {
        $this->meta = $meta;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->_links;
    }

    /**
     * @param array $links
     */
    public function setLinks($links): void
    {
        $this->_links = $links;
    }
}
