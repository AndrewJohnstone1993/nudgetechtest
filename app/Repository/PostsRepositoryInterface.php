<?php
namespace App\Repository;

interface PostsRepositoryInterface
{
    public function getPosts();

    public function getPostsByCategory(int $category);
}
