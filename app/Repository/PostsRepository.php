<?php
namespace App\Repository;

use App\Model\WordPressPost;
use GuzzleHttp\Client;

class PostsRepository implements PostsRepositoryInterface {

    const BASE_URI = 'https://www.black-ink.org/';
    const POST_END_POINT = 'wp-json/wp/v2/posts';

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $wordPressPosts;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => self::BASE_URI]);
        $response = $this->client->request('GET', self::POST_END_POINT);
        $content = $response->getBody()->getContents();

        $jsonArray = json_decode($content, TRUE);

        foreach ($jsonArray as $json) {
            $post = new WordPressPost($json);
            $this->wordPressPosts[] = $post;
        }
    }

    /**
     * @return array
     */
    public function getPosts(): array
    {
        return $this->wordPressPosts;
    }

    /**
     * @param int $category
     * @return array $posts
     */
    public function getPostsByCategory(int $category): array
    {
        $posts = [];

        /**
         * @var $wordPressPost WordPressPost
         */
        foreach ($this->wordPressPosts as $wordPressPost) {
            if(in_array($category, $wordPressPost->getCategories())) {
                $posts[] = $wordPressPost;
            }
        }

        return $posts;
    }
}