<?php

namespace App\Factory;

use App\Model\LinkMetaData;
use DOMDocument;
use GuzzleHttp\Client;

class LinkMetadataFactory
{
    /**
     * @param $url
     * @return LinkMetaData
     */
    public static function create($url): LinkMetaData
    {
        $client = new Client();
        $response = $client->get($url);

        $tags = get_meta_tags($url);

        return new LinkMetaData(
                        $url,
                        self::getMetaDescription($tags),
                        self::getTitle($response),
                        self::getKeywords($tags),
                        self::getSize($response)
        );
    }

    /**
     * @param $tags
     * @return string
     */
    public static function getMetaDescription($tags): string
    {
        $metaDescription = '';
        if (array_key_exists("description", $tags)) {
            $metaDescription = $tags["description"];
        }

        return $metaDescription;
    }

    /**
     * @param $response
     * @return string
     */
    public static function getTitle($response): string
    {
        $title = '';
        $html = $response->getBody()->getContents();

        $dom = new DOMDocument();

        libxml_use_internal_errors(true);
        if ($dom->loadHTML($html)) {
            $list = $dom->getElementsByTagName("title");
            if ($list->length > 0) {
                $title = $list->item(0)->textContent;
            }
        }
        libxml_use_internal_errors(false);

        return $title;
    }

    /**
     * @param $tags
     * @return string
     */
    public static function getKeywords($tags): string
    {
        $keywords = '';
        if (array_key_exists("keywords", $tags)) {
            $keywords = $tags["keywords"];
        }

        return $keywords;
    }

    /**
     * @param $response
     * @return float
     */
    public static function getSize($response): float
    {
        return $response->getBody()->getSize();
    }
}