<?php

namespace App\Http\Controllers;

use App\Factory\LinkMetadataFactory;
use App\Model\WordPressPost;
use App\Repository\PostsRepository;
use http\Url;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;

class PostsJsonController extends Controller
{

    /**
     * @var PostsRepository
     */
    protected $posts;

    /**
     * @var LinkMetadataFactory
     */
    protected $linkMetadataFactory;

    /**
     * @param  PostsRepository $posts
     * @param LinkMetadataFactory $linkMetadataFactory
     */
    public function __construct(
        PostsRepository $posts,
        LinkMetadataFactory $linkMetadataFactory
    )   {
        $this->posts = $posts;
        $this->linkMetadataFactory = $linkMetadataFactory;
    }

    /**
     * @return JsonResponse
     */
    function viewAction(): JsonResponse
    {
        $postsInCategory = $this->posts->getPostsByCategory(3);

        $links = [];
        /**
         * @var WordPressPost $post
         */
        foreach ($postsInCategory as $post) {
            $links = array_merge($links, $post->getUriFromContent());
        }

        $metaDataObjects = [];
        $totalSize = 0;

        foreach ($links as $link) {
            $linkMetaDataObject = $this->linkMetadataFactory->create($link);
            $metaDataObjects[] = $linkMetaDataObject;

            $totalSize = $totalSize + $linkMetaDataObject->getSize();
        }

        $output = new stdClass();
        $output->results = $metaDataObjects;
        $output->total = $totalSize . ' KB';

        return response()->json($output,200,[],JSON_PRETTY_PRINT);
    }
}